const Course = require("../models/Course");


module.exports.getAllArchvieCourses = () =>{
	return Course.find({}).then(result =>{
		return result;
	});
};



module.exports.getAllActive  =() => {
	return Course.find({isAdmin:true }).then(result =>{
		return result;
	});
};

module.exports.patchCourse = (reqParams)=>{
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};


module.exports.getCourse = (reqParams)=>{
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};


module.exports.updateCourse = (reqParams, reqBody) => {
	let updateCourse = {
 		name: reqBody.name,
 		description: reqBody.description,
 		price: reqBody.price
 	};

 	return Course.findByIdAndUpdate(reqParams.courseId,
 		updateCourse).then((course, error) => {
 			if(error){
 				return false;
 			} else {
 				return true
 			};
 		});
	};


