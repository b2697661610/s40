
/*
const Course = require("../models/course");



const bcrypt = require("bcrypt");

const auth = require("../auth");



// create a new course
module.exports.addCourse = (data) => {
	if(data.isAdmin){
	let newCourse = new course ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price	
	});
	return newCourse.save().then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
};

module.exports.loginAdmin = (reqBody) => {
 	return Admin.findOne({email: reqBody.email}).then (result =>
 		{

 			if(result == null){
 				return false
 			}else{
 				const isPasswordCorrect = bcrypt.compareSync(
 					reqBody.password, result.password);
 				if(isPasswordCorrect){
 					return{access: auth.createAccessToken(result)
 				}
 				}else{
 					return  false;
 				};
 			};
 		});
 };
*/

const Course = require("../models/Course");

// Create a new course
// module.exports.addCourse = (reqBody) => {
// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	});
// 	return newCourse.save().then((course, error) => {
// 		// Course creation failed
// 		if (error) {
// 			return false;
// 		// Course creation successful
// 		} else {
// 			return true;
// 		};
// 	});
// };

module.exports.addCourse = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});
		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});
	// User is not an admin
	};

	let message = Promise.resolve(`User must be ADMIN to access this!`);
	return message.then((value) =>{
		return {value};
	});
};


// Retrieve ALL courses
module.exports.getAllCourses = () =>{
	return Course.find({}).then(result =>{
		return result;
	});
};



// Retrieve ACTIVE courses

module.exports.getAllActive  =() => {
	return Course.find({isAdmin:true }).then(result =>{
		return result;
	});
};

// Retrieve specific course
module.exports.getCourse = (reqParams)=>{
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Update a Course

module.exports.updateCourse = (reqParams, reqBody) => {
	let updateCourse = {
 		name: reqBody.name,
 		description: reqBody.description,
 		price: reqBody.price
 	};

 	return Course.findByIdAndUpdate(reqParams.courseId,
 		updateCourse).then((course, error) => {
 			if(error){
 				return false;
 			} else {
 				return true
 			};
 		});
	};


