const express = require("express");
const router = express.Router();


const courseController = require("../controllers/courseArchiveController");

const auth = require("../auth");

router.patch("/:courseId/archive", (req,res) => {
	courseArchiveController.updateArchiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

	
module.exports = router;
